import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MapComponent } from './components/map/map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MapService } from './_services/map.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { HomeComponent } from './components/home';
import { LoginComponent } from './components/login';
import { RegisterComponent } from './components/register';
import { AfterRegisterComponent } from './components/after_register';
import { EmailConfirmationComponent } from './components/email_confirmation';
import { EditUserDataComponent } from './components/edit_user_data';
import { ChangePasswordComponent } from './components/change_password/change_password.component';
import { ResettingPasswordComponent } from './components/resetting_password';
import { ResettingPasswordConfirmComponent } from './components/resetting_password_confirm';
import { VenuesComponent } from './components/venues';
import { AddVenueComponent} from './components/add_venue';
import { VenueComponent } from './components/venue';
import { CreateEventComponent } from './components/create_event';
import { EventsComponent } from './components/events';
import { EventComponent } from './components/event';

import { AccordionModule } from 'primeng/accordion';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CardModule } from 'primeng/card';
import { CheckboxModule } from 'primeng/checkbox';
import { ScrollPanelModule } from 'primeng/scrollpanel';

import { MenuComponent } from './components/menu/menu.component';
import { AutoCompleteModule, ButtonModule, InputTextModule, OverlayPanelModule, TooltipModule, FieldsetModule} from 'primeng/primeng';
import { CalendarModule } from 'primeng/calendar';
import { HttpService } from './_services/http.service';
import {PwaService} from './_services/pwa.service';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AfterRegisterComponent,
    MenuComponent,
    EmailConfirmationComponent,
    EditUserDataComponent,
    ChangePasswordComponent,
    ResettingPasswordComponent,
    ResettingPasswordConfirmComponent,
    VenuesComponent,
    AddVenueComponent,
    VenueComponent,
    CreateEventComponent,
    EventsComponent,
    EventComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    LeafletModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    AccordionModule,
    ProgressSpinnerModule,
    BrowserAnimationsModule,
    MessagesModule,
    MessageModule,
    CardModule,
    TooltipModule,
    OverlayPanelModule,
    ButtonModule,
    InputTextModule,
    AutoCompleteModule,
    DialogModule,
    CheckboxModule,
    ScrollPanelModule,
    FieldsetModule,
    CalendarModule,
  ],
  providers: [
    MapService,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    HttpService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    PwaService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
