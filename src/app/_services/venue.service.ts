import { latLng } from 'leaflet';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { VenueModel, VenueListRespons, Venue } from '../_models';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class VenuesService {
  constructor(private http: HttpClient) { }

  getList(limit: number, offset: number) {
    return this.http.get<VenueListRespons>(`${environment.apiUrl}/venues`, { params: {limit: `${limit}`, offset: `${offset}`}});
  }

  getListForName(limit: number, offset: number, name: string) {
    return this.http.get<VenueListRespons>(`${environment.apiUrl}/venues`, { params: {limit: `${limit}`, offset: `${offset}`, name: name}});
  }

  getListForLocation(limit: number, offset: number, location: number[], radius: number) {
    return this.http.get<VenueListRespons>(`${environment.apiUrl}/venues`, { params: {limit: `${limit}`, offset: `${offset}`, longitude: `${location[1]}`, latitude: `${location[0]}`, radius: `${radius}` }});
  }

  getById(id: string) {
    return this.http.get<Venue>(`${environment.apiUrl}/venues/${id}`);
  }

  addVenue(venue: any) {
    return this.http.post(`${environment.apiUrl}/venues/`, venue);
  }

  update(id: number, venue: any) {
    return this.http.patch(`${environment.apiUrl}/venues/${id}`, venue);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/venues/${id}`);
  }
}
