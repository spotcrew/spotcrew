import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { VenueModel } from '../_models/venue.model';
import { latLng } from "leaflet";
import {VenuesService} from "./venue.service";

@Injectable({
  providedIn: 'root'
})
export class MapService {
  venues: BehaviorSubject<Array<VenueModel>> = new BehaviorSubject<Array<VenueModel>>([]);
  position: BehaviorSubject<any> = new BehaviorSubject(null);
  map;
  constructor(private venuesService: VenuesService) { }
  // to do - pobieranie listy obiektów dla danej lokalizacji
  getVenues() {
    let location = [this.map.getCenter().lat, this.map.getCenter().lng];
    this.venuesService.getListForLocation(10, 0, location, 10)
      .subscribe((data: any) => {
        //console.log(data);
        this.venues.next(data.results);
      });
  }
  // określenie lokalizacji uzytkownika
  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        this.position.next(position);
        return 0;
      });

    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }

  getCurrentPosition() {
      return latLng(this.position.getValue().coords.latitude, this.position.getValue().coords.longitude);
  }

  setMap(map) {
    this.map = map;
  }
  isMapReady(){
    return this.map.is
  }
  centerMap(lat, lng) {
    this.map.setView(latLng(lat, lng), 14);
  }
  getMapCenter() {
    if(this.map!= undefined)
      return this.map.getCenter();
    else
      return null;
  }
  findVenues() {
  }


}
