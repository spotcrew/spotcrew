import { Event } from '../_models';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class EventService {
  constructor(private http: HttpClient) { }

  getList() {
    return this.http.get<Event[]>(`${environment.apiUrl}/events`);
  }

  getById(id: string) {
    return this.http.get<Event>(`${environment.apiUrl}/events/${id}`);
  }

  createEvent(venue: any) {
    return this.http.post(`${environment.apiUrl}/events/`, venue);
  }

  update(id: number, venue: any) {
    return this.http.patch(`${environment.apiUrl}/events/${id}`, venue);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/events/${id}`);
  }

  attendanceCreateToEvent(event_id: number, user_id: number) {
    return this.http.post(`${environment.apiUrl}/events/${event_id}/attendance/`, { event_id, user_id });
  }

  attendanceDeleteToEvent(event_id: number) {
    return this.http.delete(`${environment.apiUrl}/events/${event_id}/attendance/`);
  }
}
