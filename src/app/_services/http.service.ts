import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { LatLng } from "leaflet";

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  getLocalizationResults(query: string, latLng: LatLng) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get('http://places.cit.api.here.com/places/v1/autosuggest?at='
    + latLng.lat + ',' + latLng.lng + '&q=' + query + '&app_id=IkVG1gMYxHv0fDCrrxDl&app_code=G0EaPB8vFmovxk__QLZXXw',
      { headers: headers });
  }

  emailActivation(uid: string, token: string) {
    const url = `${environment.apiUrl}/token/activation/${uid}/${token}/`;
    return this.http.get(url);
  }

  createResettingPassword(email: string) {
    return this.http.post<any>(`${environment.apiUrl}/resetting_password/`, { email });
  }

  confirmResettingPassword(password: string, token: string) {
    return this.http.post<any>(`${environment.apiUrl}/resetting_password/confirm/`, { password, token });
  }

  refreshingToken(refresh: string) {
    return this.http.post<any>(`${environment.apiUrl}/token/refreshing/`, { refresh });
  }

}
