import { JwtDecode } from './../_helpers/jwt_decode';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { User } from '../_models';
import { LoginRespons } from '../_models';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    private jwtDecode = new JwtDecode();

    constructor(
      private http: HttpClient,
      private router: Router
      ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/login/`, { username, password })
            .pipe(switchMap((loginRespons: LoginRespons) => {
                // login successful if there's a jwt token in the response
                const user = new User();
                if (loginRespons && loginRespons.access) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    console.log(loginRespons);
                    const tokenInfo = this.jwtDecode.getDecodedAccessToken(loginRespons.access); // decode token
                    user.id = tokenInfo.user_id;
                    user.access = loginRespons.access;
                    user.refresh = loginRespons.refresh;
                }

                return this.http.get<any>(`${environment.apiUrl}/users/` + user.id ) // second api call
                // need some mode logic after second API call is done? call first()?
                // map the result with a pipe
                  .pipe(map((userData: User) => {
                        // If you aren't manipulating the data
                        // to be returned, then use tap() instead of map().
                        user.email = userData.email;
                        user.username = userData.username;
                        user.avatar_link = userData.avatar_link;
                        localStorage.setItem('currentUser', JSON.stringify(user));
                        this.currentUserSubject.next(user);
                      return user;
                  }));
            }));
    }

    updateUserData(user: User) {
        return this.http.get<any>(`${environment.apiUrl}/users/` + user.id ) //second api call
                // need some mode logic after second API call is done? call first()?
                // map the result with a pipe
                  .pipe(map((userData: User) => {
                        let newUser = user;
                        newUser.email = userData.email;
                        newUser.username = userData.username;
                        localStorage.setItem('currentUser', JSON.stringify(newUser));
                        this.currentUserSubject.next(newUser);
                      return newUser;
                  }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.router.navigate(['login']);
    }
}
