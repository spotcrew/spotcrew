import { Sport } from '../_models';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class SportService {
  constructor(private http: HttpClient) { }

  getList() {
    return this.http.get<Sport[]>(`${environment.apiUrl}/sports`);
  }

  getById(id: string) {
    return this.http.get<Sport>(`${environment.apiUrl}/sports/${id}`);
  }

  addSport(venue: any) {
    return this.http.post(`${environment.apiUrl}/sports/`, venue);
  }

  update(id: number, venue: any) {
    return this.http.patch(`${environment.apiUrl}/sports/${id}`, venue);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/sports/${id}`);
  }

  returnSportImage(id: number) {
    switch (id) {
      case 2: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164183/SPORTS/frame-harirak-602126-unsplash.jpg";
      }
      case 3: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164188/SPORTS/matthew-t-rader-1235613-unsplash.jpg";
      }
      case 4: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164181/SPORTS/vince-fleming-1109901-unsplash.jpg";
      }
      case 5: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164191/SPORTS/persnickety-prints-1405186-unsplash.jpg";
      }
      case 6: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164168/SPORTS/ben-hershey-575578-unsplash.jpg";
      }
      case 7: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164174/SPORTS/joshua-hoehne-586199-unsplash.jpg";
      }
      case 8: {
        return "https://res.cloudinary.com/photop/image/upload/v1559197380/SPORTS/marino-bobetic-1467017-unsplash.jpg";
      }
      case 9: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164196/SPORTS/chris-liverani-510544-unsplash.jpg";
      }
      case 9: {
        return "https://res.cloudinary.com/photop/image/upload/v1559197553/SPORTS/bruce-warrington-1435476-unsplash.jpg";
      }
      default: {
        return "https://res.cloudinary.com/photop/image/upload/v1559164198/SPORTS/photo-1547242225-b7eaf7c97673.jpg";
      }
    }
  }
}
