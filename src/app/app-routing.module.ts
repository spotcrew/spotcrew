import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapComponent } from './components/map/map.component';
import { HomeComponent } from './components/home';
import { LoginComponent } from './components/login';
import { RegisterComponent } from './components/register';
import { AfterRegisterComponent } from './components/after_register';
import { AuthGuard } from './_guards';
import { EmailConfirmationComponent } from './components/email_confirmation';
import { EditUserDataComponent } from './components/edit_user_data';
import { ChangePasswordComponent } from './components/change_password/change_password.component';
import { ResettingPasswordComponent } from './components/resetting_password';
import { ResettingPasswordConfirmComponent } from './components/resetting_password_confirm';
import { VenuesComponent } from './components/venues';
import { AddVenueComponent } from './components/add_venue';
import { VenueComponent } from './components/venue';
import { CreateEventComponent } from './components/create_event';
import { EventsComponent } from './components/events';
import { EventComponent } from './components/event';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'map', component: MapComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'after-register', component: AfterRegisterComponent },
  { path: 'email-confirmation', component: EmailConfirmationComponent },
  { path: 'edit-user-data', component: EditUserDataComponent, canActivate: [AuthGuard]  },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard]  },
  { path: 'create-resetting-password', component: ResettingPasswordComponent },
  { path: 'resetting_password', component: ResettingPasswordConfirmComponent },
  { path: 'venues', component: VenuesComponent, canActivate: [AuthGuard]  },
  { path: 'add-venue', component: AddVenueComponent, canActivate: [AuthGuard] },
  { path: 'venue/:id', component: VenueComponent, canActivate: [AuthGuard]},
  { path: 'create-event', component: CreateEventComponent, canActivate: [AuthGuard]},
  { path: 'events', component: EventsComponent, canActivate: [AuthGuard]},
  { path: 'event/:id', component: EventComponent, canActivate: [AuthGuard]},
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
