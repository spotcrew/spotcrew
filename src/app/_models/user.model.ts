export class User {
  id: number;
  email: string;
  username: string;
  password: string;
  access: string;
  refresh: string;
  avatar_link: string;
}
