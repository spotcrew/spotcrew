import { LatLng } from 'leaflet';

export class Location {
  coordinates: number[];
  type: String;
}
