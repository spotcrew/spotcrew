export class EditUserDataRespons {
  username: string;
  email: string;
  avatar_link: string;
}
