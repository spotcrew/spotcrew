// model reprezentujący klase obiektu sportowego

export class VenueModel {
  idVenue;
  name;
  description;
  latitude;
  longitude;
  city;
  address;
  image;
  constructor(idVenue, name, description, latitude, longitude , city = 'Poznań', address = 'Poznańska 1',  image = 'fakepath') {
    this.idVenue = idVenue;
    this.name = name;
    this.description = description;
    this.latitude = latitude;
    this.longitude = longitude;
    this.city = city;
    this.address = address;
    this.image = image;
  }
}
