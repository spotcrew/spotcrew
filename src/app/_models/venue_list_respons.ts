import { Venue } from './venue2.model';

export class VenueListRespons {
  count: number;
  next: string;
  privious: string;
  results: Venue[];
}
