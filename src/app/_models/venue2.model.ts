import { Location } from './location.model';

export class Venue {
  venue_id: number;
  name: string;
  address: string;
  city: string;
  description: string;
  images: Image[];
  location: Location;
  distance_from_you: number;
}

export class Image {
  link: string;
}
