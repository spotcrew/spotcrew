import { User } from '.';

export class Event {
  event_id: number;
  users: User[];
  name: string;
  description: string;
  event_start: string;
  event_stop: string;
  venue_id: number;
  host_id: number;
  sport_id: number;
  sport_image: string;
  sport_name: string;
}
