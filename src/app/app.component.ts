import {Component} from '@angular/core';
import {AuthenticationService} from './_services';
import {User} from './_models';
import {PwaService} from './_services/pwa.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Spotcrew';

  isUserLogged = false;
  btnAdd = document.getElementById('btnAdd');
  deferredPrompt;

  constructor(private authService: AuthenticationService, private pwa: PwaService) {
    this.authService.currentUser.subscribe((res: User) => {
      if (res != null) {
        this.isUserLogged = true;
      } else {
        this.isUserLogged = false;
      }
    });
    this.btnAdd = document.getElementById('btnAdd');


    //   window.addEventListener('beforeinstallprompt', (e) => {
    //     console.log('beforeinstallprompt');
    //     // Prevent Chrome 67 and earlier from automatically showing the prompt
    //     e.preventDefault();
    //     // Stash the event so it can be triggered later.
    //     this.deferredPrompt = e;
    //     // Update UI notify the user they can add to home screen
    //     this.btnAdd.style.display = 'block'
    //   });
    //
    //   window.addEventListener('appinstalled', (evt) => {
    //     console.log('appinstaled');
    //   });
    // }
    //
    // addToHomeScreen(event, btnAdd) {
    //   // hide our user interface that shows our A2HS button
    //   btnAdd.style.display = 'none';
    //   // Show the prompt
    //   this.deferredPrompt = confirm('Add to home screen?');
    //   // Wait for the user to respond to the promp
    //   if (this.deferredPrompt === true) {
    //     console.log('User accepted the A2HS prompt');
    //   } else {
    //     console.log('User dismissed the A2HS prompt');
    //   }
    //   this.deferredPrompt = null;
    // }
  }

  installPwa(): void {
    this.pwa.promptEvent.prompt();
  }
}
