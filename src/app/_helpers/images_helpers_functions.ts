import { Image } from './../_models';
import { Injectable } from '@angular/core';
@Injectable({ providedIn: 'root' })
export class ImagesHelpersFunctions {

  showImage(link: string, p1: number, p2: number, w: number, type: string, token: number = 2): string {

    const params = `w_${w},ar_${p1}:${p2},c_fill,g_auto`;

    if (link != null) {
      const tokens = link.split('/');
      console.log(tokens);
      if (tokens[2] === 'res.cloudinary.com' && tokens[3] === 'photop') {
        tokens.splice(-token, 0, params);
        return tokens.join('/');
      } else {
        return link;
      }
    } else {
      switch (type) {
        case 'avatar': {
          return `https://res.cloudinary.com/photop/image/upload/${params}/v1558442824/SPOTCREW/avatar.png`;
        }
        case 'venue': {
          return `https://res.cloudinary.com/photop/image/upload/${params}/v1558442824/SPOTCREW/venue2.jpg`;
        }
        default: {
          return `https://res.cloudinary.com/photop/image/upload/${params}/v1558442824/SPOTCREW/avatar.png`;
        }
      }
    }
  }

  showImageForImagesArray(array: Image[], p1: number, p2: number, w: number, type: string, token: number = 2): Image[] {

    if (array.length === 0) {
      const image = new Image();
      image.link = this.showImage(null, p1, p2, w, type, token);
      array.push(image);
    } else {
      const link = this.showImage(array[0].link, p1, p2, w, type, token);
      array[0].link = link;
    }

    return array;
  }

  showImageForImagesArrayAll(array: Image[], p1: number, p2: number, w: number, type: string, token: number = 2): Image[] {

    if (array.length === 0) {
      const image = new Image();
      image.link = this.showImage(null, p1, p2, w, type, token);
      array.push(image);
    } else {
      array.forEach( image => {
        const link = this.showImage(image.link, p1, p2, w, type, token);
        image.link = link;
      });
    }

    return array;
  }
}
