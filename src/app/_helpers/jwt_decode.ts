import * as jwt_decode from 'jwt-decode';

export class JwtDecode {

  getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    } catch (Error) {
        return null;
    }
  }
}

