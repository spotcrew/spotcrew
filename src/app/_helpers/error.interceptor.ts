import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        // let currentUser: User = null;
        // let currentUserSubscription: Subscription = null;
        // currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        //   currentUser = user;
        // });
        // this.httpService.refreshingToken(currentUser.refresh)
        //   .subscribe(
        //     data => {
        //       console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        //       console.log(data);
        //     });
        this.authenticationService.logout();
        location.reload(true);
      }

      //register
      let error = err.statusText;
      //console.log(err);//https://dominpn.pythonanywhere.com/api/users/
      if (err.url == "https://dominpn.pythonanywhere.com/api/users/") {
        error = ''
        if (err.error.email !== undefined) {
          if (err.error.email[0] !== undefined) { error += ' ➤' + err.error.email[0]; }
        }
        if (err.error.username !== undefined) {
          if (err.error.username[0] !== undefined) { error += ' ➤' + err.error.username[0]; }
        }
        if (error === '') { error = err.statusText; }
      } else {
        error = err.error.message || err.error.msg || err.error.non_field_errors || err.statusText;
      }
      return throwError(error);
    }))
  }
}
