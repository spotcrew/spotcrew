import { Venue } from './../../_models/venue2.model';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User, EditUserDataRespons, Location } from '../../_models';
import { UserService, AlertService, AuthenticationService, VenuesService } from '../../_services';
import { latLng, marker, tileLayer, LatLngExpression } from 'leaflet';
import * as L from 'leaflet';
import { MapService } from '../../_services/map.service';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: 'add_venue.component.html',
  styleUrls: ['./add_venue.component.css']
})
export class AddVenueComponent implements OnInit {
  currentUser: User;
  currentUserSubscription: Subscription;

  addVenueForm: FormGroup;
  loading = false;
  submitted = false;

  select_file: any[] = [null, null, null, null, null];
  image_link: string[] = ['', '', '', '', ''];
  file_name: String[] = ['', '', '', '', ''];
  uploadFile: boolean[] = [false, false, false, false, false];

  valid_type: boolean[] = [false, false, false, false, false];
  valid_size: boolean[] = [false, false, false, false, false];

  options = {
    layers: [
      // tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      //   attribution: '&copy; OpenStreetMap contributors'
      // }),
      tileLayer('https://2.base.maps.api.here.com/maptile/2.1/maptile/newest/reduced.day/{z}/{x}/{y}/512/png8?app_id=' +
        'IkVG1gMYxHv0fDCrrxDl' + '&app_code=' + 'G0EaPB8vFmovxk__QLZXXw' + '&ppi=320'
      )

    ],
    zoom: 7,
    center: latLng([52, 19])
  };

  layers = [];  // tablica ze znacznikami
  map;  // obiekt reprezentujący mape, dzięki czemu można działać na mape z poziomu typescript

  tmpLocation: any = null;
  latLng: L.LatLng = null;
  select_location = true;

  markerIconUser = L.icon({
    iconUrl: './assets/icons/usermarker.png',

    iconSize: [32, 32], // size of the icon
    iconAnchor: [16, 16], // point of the icon which will correspond to marker's location
    popupAnchor: [16, 0] // point from which the popup should open relative to the iconAnchor
  });

  markerLocation = L.icon({
    iconUrl: './assets/icons/location.png',

    iconSize: [32, 32], // size of the icon
    iconAnchor: [16, 32], // point of the icon which will correspond to marker's location
    popupAnchor: [16, 0], // point from which the popup should open relative to the iconAnchor
  });

  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private mapService: MapService,
    private venueService: VenuesService,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });

    this.mapService.position.subscribe((pos: Position) => {
      if (pos != null) {
        this.markMe(pos.coords.latitude, pos.coords.longitude);
      }
    });
    this.mapService.findMe();
  }

  ngOnInit() {

    this.addVenueForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: [''],
      city: ['', [Validators.required, Validators.minLength(3)]],
      address: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  markMe(lat, lng) {
    this.centerMap(lat, lng);
    this.addMarker(lat, lng, 'Your location', '', this.markerIconUser);
  }

  refreshLocalization() {
    this.mapService.findMe();
  }

  onMapReady(map: L.Map) {
    // this.map = map;
    this.mapService.setMap(map);
    this.mapService.findMe();
  }

  selectCoordinates(event) {
    if (this.tmpLocation != null) {
      this.tmpLocation.setLatLng(event.latlng);
    } else {
      this.tmpLocation = marker(event.latlng).setIcon(this.markerLocation);
      this.layers.push(this.tmpLocation);
    }
    this.latLng = latLng(event.latlng.lat, event.latlng.lng);
    this.select_location = false;
  }

  // wyśrodkowanie mapy w danym punkcie
  centerMap(lat, lng) {
    this.mapService.centerMap(latLng(lat, lng), 14);
  }

  // dodawanie znacznika do mapy
  addMarker(lat, lng, name, description = null, markerIcon) {
    this.layers.push(marker([lat, lng]).bindPopup('<b>' + name + '</b><br>' + description + '<br>').setIcon(markerIcon));
  }
  // to do - pobieranie listy venues
  findVenues() {
    this.mapService.findVenues();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.addVenueForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addVenueForm.invalid) {
      return;
    }

    for (let i = 0; i < 5; i++) {
      if (this.valid_size[i] === true || this.valid_type[i] === true) {
        return;
      }
    }

    console.log(this.addVenueForm.value);

    this.loading = true;

    const obj: { [k: string]: any } = {};
    obj.name = this.f.name.value;
    if (this.f.description.value !== '') { obj.description = this.f.description.value; }
    obj.city = this.f.city.value;
    obj.address = this.f.address.value;
    obj.location = `{"type": "Point", "coordinates": [${this.latLng.lng}, ${this.latLng.lat}]}`;

    let links = new Array();
    this.image_link.forEach(element => {
      if (element !== '') {
        const link: { [k: string]: any } = {};
        link.link = element;
        links.push(link);
      }
    });
    obj.images = links;

    this.venueService.addVenue(obj)
      .pipe(map((venue: Venue) => {
        console.log(venue);
      }))
      .subscribe(
        data => {
          this.alertService.success('Add successful', true);
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  onFileSelected(event, nr) {
    this.select_file[nr] = event.target.files[0];
    this.file_name[nr] = this.select_file[nr].name;

    if (this.select_file[nr].size > 1000000) {
      this.valid_size[nr] = true;
      return;
    } else {
      this.valid_size[nr] = false;
    }

    if (this.select_file[nr].type !== 'image/jpeg' && this.select_file[nr].type !== 'image/png') {
      this.valid_type[nr] = true;
      return;
    } else {
      this.valid_type[nr] = false;
    }

    this.uploadFile[nr] = true;
    const url = `https://api.cloudinary.com/v1_1/${environment.cloudName}/upload`;
    const xhr = new XMLHttpRequest();
    const fd = new FormData();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    // Update progress (can be used to show progress indicator)
    xhr.upload.addEventListener("progress", function (e) {
      console.log(`fileuploadprogress data.loaded: ${e.loaded},
  data.total: ${e.total}`);
    });

    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          const response = JSON.parse(xhr.responseText);
          console.log(response);
          this.image_link[nr] = response.secure_url;
          this.uploadFile[nr] = false;
        } else {
          this.alertService.error(xhr.response);
          this.uploadFile[nr] = false;
        }
      }
    };

    fd.append('upload_preset', environment.unsignedUploadPreset);
    fd.append('tags', 'browser_upload'); // Optional - add tag for image admin in Cloudinary
    fd.append('file', this.select_file[nr]);
    xhr.send(fd);

  }

  selectNumberOfPhotos(event) {
    console.log(event.target.value);
  }

}
