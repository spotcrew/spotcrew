import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User, EditUserDataRespons } from '../../_models';
import { UserService, AlertService, AuthenticationService } from '../../_services';

import { environment } from 'src/environments/environment';

@Component({
  templateUrl: 'edit_user_data.component.html',
  styleUrls: ['./edit_user_data.component.css']
})
export class EditUserDataComponent implements OnInit {
  currentUser: User;
  currentUserSubscription: Subscription;

  editUserDataForm: FormGroup;
  loading = false;
  submitted = false;

  select_file: any;
  file_name = '';

  valid_type = false;
  valid_size = false;

  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {

    this.editUserDataForm = this.formBuilder.group({
      username: [''],
      email: ['', Validators.email],
      avatar: [null]
    });

  }

  // convenience getter for easy access to form fields
  get f() {
    return this.editUserDataForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editUserDataForm.invalid) {
      return;
    }

    if ( this.valid_size === true || this.valid_type === true){
      return;
    }

    console.log(this.editUserDataForm.value);

    if (this.select_file != null) {

      this.loading = true;
      var url = `https://api.cloudinary.com/v1_1/${environment.cloudName}/upload`;
      var xhr = new XMLHttpRequest();
      var fd = new FormData();
      xhr.open('POST', url, true);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

      // Update progress (can be used to show progress indicator)
      xhr.upload.addEventListener("progress", function (e) {
        console.log(`fileuploadprogress data.loaded: ${e.loaded},
  data.total: ${e.total}`);
      });

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            console.log(xhr.response);
            const obj: { [k: string]: any } = {};
            if (this.f.username.value !== '') { obj.username = this.f.username.value; }
            if (this.f.email.value !== '') { obj.email = this.f.email.value; }
            var response = JSON.parse(xhr.responseText);
            console.log(response);
            obj.avatar_link = response.secure_url;
            this.userService.update(this.currentUser.id, obj)
              .pipe(map((editUserDataRespons: EditUserDataRespons) => {
                console.log(editUserDataRespons);
                this.currentUser.email = editUserDataRespons.email;
                this.currentUser.username = editUserDataRespons.username;
                this.currentUser.avatar_link = editUserDataRespons.avatar_link;
                localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
              }))
              .subscribe(
                data => {
                  this.alertService.success('Edit successful', true);
                  this.loading = false;
                },
                error => {
                  this.alertService.error(error);
                  this.loading = false;
                });

          } else {
            this.alertService.error(xhr.response);
          }
        }
      };

      fd.append('upload_preset', environment.unsignedUploadPreset);
      fd.append('tags', 'browser_upload'); // Optional - add tag for image admin in Cloudinary
      fd.append('file', this.select_file);
      xhr.send(fd);
    } else {

      if (this.f.email.value === '' && this.f.username.value === '') {
        return;
      }

      const obj: { [k: string]: any } = {};
      if (this.f.username.value !== '') { obj.username = this.f.username.value; }
      if (this.f.email.value !== '') { obj.email = this.f.email.value; }
      this.userService.update(this.currentUser.id, obj)
        .pipe(map((editUserDataRespons: EditUserDataRespons) => {
          console.log(editUserDataRespons);
          this.currentUser.email = editUserDataRespons.email;
          this.currentUser.username = editUserDataRespons.username;
          this.currentUser.avatar_link = editUserDataRespons.avatar_link;
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        }))
        .subscribe(
          data => {
            this.alertService.success('Edit successful', true);
            this.loading = false;
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
    }

  }

  onFileSelected(event) {
    this.select_file = event.target.files[0];
    this.file_name = this.select_file.name;

    if (this.select_file.size > 1000000){
      this.valid_size = true;
    } else {
      this.valid_size = false;
    }

    if (this.select_file.type !== 'image/jpeg' && this.select_file.type !== 'image/png'){
      this.valid_type = true;
    } else {
      this.valid_type = false;
    }

  }

}
