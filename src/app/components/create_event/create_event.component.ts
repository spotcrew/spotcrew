import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User, EditUserDataRespons, Location, Sport, Venue, Image } from '../../_models';
import { UserService, AlertService, AuthenticationService, VenuesService, SportService, EventService } from '../../_services';
import { latLng, marker, tileLayer, LatLngExpression } from 'leaflet';
import * as L from 'leaflet';
import { MapService } from '../../_services/map.service';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: 'create_event.component.html',
  styleUrls: ['./create_event.component.css']
})
export class CreateEventComponent implements OnInit {
  currentUser: User;
  currentUserSubscription: Subscription;

  createEventForm: FormGroup;
  loading = false;
  read_venue_data = false;
  submitted = false;
  date7: Date;
  minStartDateValue = new Date();
  minEndDateValue = new Date();

  sports: Sport[];
  venue: Venue;

  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private venueService: VenuesService,
    private route: ActivatedRoute,
    private sportService: SportService,
    private eventService: EventService,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.createEventForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: [''],
      event_start: [this.minStartDateValue],
      event_stop: [this.minStartDateValue],
      venue_id: [this.route.snapshot.queryParamMap.get('venue_id')],
      host_id: [this.currentUser.id],
      sport_id: ['', [Validators.required]]
    });

    this.sportService.getList()
      .subscribe(
        data => {
          this.sports = data;
          console.log(this.sports);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

    this.venueService.getById(this.route.snapshot.queryParamMap.get('venue_id'))
      .subscribe(data => {
        console.log(data);
        this.venue = data;
        if (this.venue.images.length === 0) {
          const image = new Image();
          image.link = 'https://res.cloudinary.com/photop/image/upload/w_500,ar_2:1,c_fill,g_auto/v1558442824/SPOTCREW/venue2.jpg';
          this.venue.images.push(image);
        } else {
          const tokens = this.venue.images[0].link.split('/');
          if (tokens[2] === 'res.cloudinary.com' && tokens[3] === 'photop') {
            const params = 'w_500,ar_2:1,c_fill,g_auto';
            tokens.splice(-2, 0, params);
            this.venue.images[0].link = tokens.join('/');
          }
        }
        this.read_venue_data = true;
      });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.createEventForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createEventForm.invalid) {
      return;
    }

    console.log(this.createEventForm.value);

    this.loading = true;

    this.eventService.createEvent(this.createEventForm.value)
      .subscribe(
        data => {
          this.alertService.success('Add successful', true);
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  onBlurMethod(event) {
    this.minEndDateValue = event;
    this.f.event_stop.setValue(event);
  }

  changeVenue() {
    this.router.navigate(['/venues']);
  }
}
