import { ImagesHelpersFunctions } from './../../_helpers/images_helpers_functions';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { Venue, Image } from '../../_models';
import { VenuesService, AuthenticationService, AlertService } from '../../_services';

@Component({ templateUrl: 'venue.component.html' })
export class VenueComponent implements OnInit {

  venue: Venue = null;

  loading = true;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private venuesService: VenuesService,
    private alertService: AlertService,
    private imagesHelpersFunctions: ImagesHelpersFunctions,
  ) {

  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.venuesService.getById(id)
      .subscribe(data => {
        console.log(data);
        this.venue = data;
        this.venue.images = this.imagesHelpersFunctions.showImageForImagesArrayAll(this.venue.images, 2, 1, 500, 'venue');
        this.loading = false;
      });
  }

  createEvent() {
    this.router.navigate(['/create-event'],  {queryParams: {venue_id: this.venue.venue_id}});
  }

}
