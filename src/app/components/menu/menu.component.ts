import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService, MapService, UserService } from "../../_services";
import { HttpService } from "../../_services/http.service";
import { OverlayPanel } from "primeng/primeng";
import { NgTemplateOutlet } from "@angular/common";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy {

  query: string;
  locationResults: Array<any> = new Array<any>();
  constructor(private router: Router, private authService: AuthenticationService, private http: HttpService, private map: MapService) { }
  ngOnInit() {
  }

  navigateTo(endpoint) {
    this.router.navigate([endpoint]);
  }

  locationSearch(event) {
    this.navigateTo('/map');
    return this.http.getLocalizationResults(event.query, this.map.getCurrentPosition()).subscribe((res: any) => {
      console.log(res);
      this.locationResults = res.results;
    })
  }
  centerMap(event, op: OverlayPanel) {
    op.hide();
    this.map.centerMap(event.position[0], event.position[1]);
    this.query = null;

  }
  logout() {
    this.authService.logout();
  }
  clearQuery() {
    this.query = '';
  }
  showSearchingPanel(event, actualTarget, op: OverlayPanel) {
    op.toggle(event, actualTarget);
  }
  ngOnDestroy() {

  }


}
