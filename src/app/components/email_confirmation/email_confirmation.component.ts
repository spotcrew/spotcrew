import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService, HttpService } from '../../_services';
import { first } from 'rxjs/operators';

@Component({ templateUrl: 'email_confirmation.component.html' })
export class EmailConfirmationComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private htmlService: HttpService,
    private alertService: AlertService) {
  }

  ngOnInit() {
    const uid = this.route.snapshot.queryParamMap.get('uid');
    const token = this.route.snapshot.queryParamMap.get('token');
    this.htmlService.emailActivation(uid, token)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Confirm email successful', true);
        },
        error => {
          this.alertService.error(error);
        });
  }

}
