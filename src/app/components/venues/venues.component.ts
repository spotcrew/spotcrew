import { ImagesHelpersFunctions } from './../../_helpers/images_helpers_functions';
import { LatLng } from 'leaflet';
import { Component, OnInit } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { OverlayPanel } from "primeng/primeng";
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { VenueModel, Venue, VenueListRespons, Image, Location } from '../../_models';
import { VenuesService, AuthenticationService, AlertService, HttpService, MapService } from '../../_services';

import { Location as Loc } from '@angular/common';

@Component({ templateUrl: 'venues.component.html' })
export class VenuesComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private venuesService: VenuesService,
    private alertService: AlertService,
    private http: HttpService,
    private map: MapService,
    private imagesHelpersFunctions: ImagesHelpersFunctions,
    private location: Loc,
    private route: ActivatedRoute
  ) {
  }

  loading = true;

  venues: Venue[];
  venues_count: number;
  query: string;
  query_name: string;

  locationResults: Array<any>;
  currentLocation: LatLng;
  position: BehaviorSubject<any> = new BehaviorSubject(null);
  limit = 10;
  offset = 0;
  lat = 0.0;
  lng = 0.0;

  ngOnInit() {

    let type = 'start';

    if (this.route.snapshot.queryParamMap.get('limit') !== null) {
      this.limit = Number(this.route.snapshot.queryParamMap.get('limit'));
    }
    if (this.route.snapshot.queryParamMap.get('offset') !== null) {
      this.offset = Number(this.route.snapshot.queryParamMap.get('offset'));
      type = 'page';
    }
    if (this.route.snapshot.queryParamMap.get('name') !== null) {
      this.query_name = this.route.snapshot.queryParamMap.get('name');
      type = 'name';
    }
    if (this.route.snapshot.queryParamMap.get('lat') !== null) {
      this.lat = Number(this.route.snapshot.queryParamMap.get('lat'));
      type = 'location';
    }
    if (this.route.snapshot.queryParamMap.get('lng') !== null) {
      this.lng = Number(this.route.snapshot.queryParamMap.get('lng'));
    }
    if (this.route.snapshot.queryParamMap.get('lng') !== null) {
      this.query = this.route.snapshot.queryParamMap.get('query');
      console.log(this.query);
    }

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        this.position.next(position);
        this.getVenueListAndShow(type);
        console.log('xd');
        return 0;
      });

    } else {
      alert('Geolocation is not supported by this browser.');
    }

  }

  getVenueListFor(type: string, distance = 30) {

    switch (type) {
      case 'start': {
        this.location.replaceState('/venues', 'limit=' + this.limit + '&' + 'offset=' + this.offset);
        return this.venuesService.getList(10, 0);
      }
      case 'name': {
        this.location.replaceState('/venues', 'limit=' + this.limit + '&offset=' + this.offset + '&name=' + this.query_name);
        return this.venuesService.getListForName(10, 0, this.query_name);
      }
    }

  }

  getVenueListAndShow(type: string) {
    this.loading = true;


    if (type === 'location') {
      this.http.getLocalizationResults(this.query, new LatLng(this.position.getValue().coords.latitude, this.position.getValue().coords.longitude))
        .subscribe((res: any) => {
          let location: number[];
          let q = '';
          if (typeof this.query !== 'string') {
            location = this.locationResults[0].position;
            q = this.locationResults[0].title;
          } else {
            location = res.results[0].position;
            q = this.query;
          }
          this.location.replaceState('/venues', 'limit=' + this.limit.toString() + '&offset=' + this.offset.toString() + '&lat=' + this.position.getValue().coords.latitude + '&lng=' + this.position.getValue().coords.longitude + '&query=' + q);
          console.log(res);
          console.log(this.query);
          return this.venuesService.getListForLocation(10, 0, location, 30)
            .subscribe(data => {
              this.venues = data.results;
              this.venues_count = data.count;
              const user_location = new LatLng(this.position.getValue().coords.latitude, this.position.getValue().coords.longitude);
              this.venues.forEach(venue => {
                venue.distance_from_you = this.calcCrow(user_location, venue.location);
                venue.images = this.imagesHelpersFunctions.showImageForImagesArray(venue.images, 2, 1, 500, 'venue');
              });
              this.loading = false;
            });
        });
    } else {
      this.getVenueListFor(type)
        .subscribe(data => {
          this.venues = data.results;
          this.venues_count = data.count;
          const user_location = new LatLng(this.position.getValue().coords.latitude, this.position.getValue().coords.longitude);
          this.venues.forEach(venue => {
            venue.distance_from_you = this.calcCrow(user_location, venue.location);
            venue.images = this.imagesHelpersFunctions.showImageForImagesArray(venue.images, 2, 1, 500, 'venue');
          });
          this.loading = false;
        });
    }

  }

  navigateTo(endpoint) {
    this.router.navigate([endpoint]);
  }

  locationSearch(event) {
    return this.http.getLocalizationResults(event.query, this.map.getCurrentPosition())
      .subscribe((res: any) => {
        this.locationResults = res.results;
      });
  }

  searchEventInLocation(event) {
    console.log(event.position);
  }

  searchVenueForLocation() {
    this.getVenueListAndShow('location');
  }

  clearQuery() {
    this.query = '';
  }

  addVenue() {
    this.router.navigate(['/add-venue']);
  }

  showDetails(id: number) {
    this.router.navigate(['/venue', id]); 
  }

  searchVenueForName() {
    this.getVenueListAndShow('name');
  }

  //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
  calcCrow(location1: LatLng, location2: Location): number {
    const R = 6371; // km
    const dLat = this.toRad(location2.coordinates[1] - location1.lat);
    const dLon = this.toRad(location2.coordinates[0] - location1.lng);
    const lat1 = this.toRad(location1.lat);
    const lat2 = this.toRad(location2.coordinates[0]);

    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d;
  }

  // Converts numeric degrees to radians
  toRad(Value: number): number {
    return Value * Math.PI / 180;
  }

  createEvent(venue_id: number) {
    this.router.navigate(['/create-event'], { queryParams: { venue_id: venue_id } });
  }
}
