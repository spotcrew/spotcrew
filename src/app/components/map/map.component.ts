import {Component, NgZone, OnInit} from '@angular/core';
import {LatLng, latLng, LeafletEventHandlerFn, marker, tileLayer} from 'leaflet';
import * as L from 'leaflet';
import {VenueModel} from '../../_models/venue.model';
import {MapService} from '../../_services/map.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  venues: Array<any> = [];
  options = {
    layers: [
      // tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      //   attribution: '&copy; OpenStreetMap contributors'
      // }),
      tileLayer('https://2.base.maps.api.here.com/maptile/2.1/maptile/newest/reduced.day/{z}/{x}/{y}/512/png8?app_id=' +
        'IkVG1gMYxHv0fDCrrxDl' + '&app_code=' + 'G0EaPB8vFmovxk__QLZXXw' + '&ppi=320'
      )

    ],
    zoom: 7,
    center: latLng([52, 19])
  };
  layers = [];  // tablica ze znacznikami
  //map;  // obiekt reprezentujący mape, dzięki czemu można działać na mape z poziomu typescript

  markerIconUser = L.icon({
    iconUrl: './assets/icons/usermarker.png',

    iconSize: [32, 32], // size of the icon
    iconAnchor: [16, 16], // point of the icon which will correspond to marker's location
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
  });
  markerIconCourt = L.icon({
    iconUrl: './assets/icons/stadium.png',

    iconSize: [32, 32], // size of the icon
    iconAnchor: [16, 16], // point of the icon which will correspond to marker's location
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
  });
  actualPosition: Position;

  constructor(private mapService: MapService, private router: Router, private ngZone: NgZone) {
  }

  ngOnInit() {
    this.mapService.findMe();


    this.mapService.position.subscribe((pos: Position) => {
      if (pos != null) {
        this.actualPosition = pos;
        this.markMeAndCenter(new LatLng(pos.coords.latitude, pos.coords.longitude));
      }
    });

    this.mapService.venues.subscribe((res: Array<any>) => {
      this.layers = [];
      if (this.actualPosition != null) {
        this.markMe(new LatLng(this.actualPosition.coords.latitude, this.actualPosition.coords.longitude));
      }
      this.addCircle();
      this.venues = res;
      this.venues.forEach(venue => {
        this.addMarkerVenue(venue, this.markerIconCourt);
      });
    });

  }

  markMe(latLng) {
    this.addMarker(latLng.lat, latLng.lng, 'Your location', '', this.markerIconUser);
  }

  // zaznaczenie znacznika na mapie reprezentującego uzytkownika
  markMeAndCenter(latLng) {
    this.centerMap(latLng);
    this.addMarker(latLng.lat, latLng.lng, 'Your location', '', this.markerIconUser);
  }

  addCircle() {
    if (this.mapService.getMapCenter() != null)
      this.layers.push(L.circle(this.mapService.getMapCenter(), {radius: 6500}));
  }

  // odświeżanie lokalizacji
  refreshLocalization() {
    this.mapService.findMe();
  }

  // event po załadowaniu mapy
  onMapReady(map: L.Map) {
    // this.map = map;
    this.mapService.setMap(map);
    this.mapService.findMe();
    this.mapService.getVenues();
  }

  // do debugowania
  showCoordinates(event) {
    console.log(event.latlng);
  }

  // wyśrodkowanie mapy w danym punkcie
  centerMap(latLng) {
    this.mapService.centerMap(latLng.lat, latLng.lng);
  }

  // dodawanie znacznika do mapy
  addMarker(lat, lng, name, description = null, markerIcon) {
    this.layers.push(marker([lat, lng])
      .bindPopup('<b>' + name + '</b><br>' + description + '<br>').setIcon(markerIcon));
  }

  addMarkerVenue(venue, markerIcon) {
    // console.log(venue.location.coordinates[0]);
    //venue.location.coordinates[1], venue.location.coordinates[0], venue.name, venue.description
    this.layers.push(marker([venue.location.coordinates[1], venue.location.coordinates[0]])
      .bindPopup(
        '<div><span><b>' + venue.name + '</b></span><br>' + venue.description +
        '<br><span>Double click on marker for details</span></div>').setIcon(markerIcon).addEventListener('dblclick', () => this.ngZone.run(() => {
        this.openVenueDetails(venue);
      }))
  );
  }

  openVenueDetails(venue) {
    this.router.navigate(['venue', venue.venue_id]);
  }

  findVenues() {
    // this.markMe(this.mapService.getCurrentPosition().lat, this.mapService.getCurrentPosition().lng);
    this.mapService.getVenues();


  }

}
