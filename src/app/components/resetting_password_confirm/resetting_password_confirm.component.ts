import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, HttpService } from '../../_services';

@Component({
  templateUrl: 'resetting_password_confirm.component.html'
})
export class ResettingPasswordConfirmComponent implements OnInit {
  resettingPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  token: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.resettingPasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    });


    this.token = this.route.snapshot.queryParamMap.get('token');
  }

  // convenience getter for easy access to form fields
  get f() { return this.resettingPasswordForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resettingPasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.httpService.confirmResettingPassword(this.f.password.value, this.token)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Resetting password successful', true);
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
