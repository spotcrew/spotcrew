import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ImagesHelpersFunctions } from '../../_helpers';
import { User } from '../../_models';
import { UserService, AuthenticationService, AlertService } from '../../_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {
  currentUser: User;
  currentUserSubscription: Subscription;

  display = false;
  avatar_url = '';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    private imagesHelpersFunctions: ImagesHelpersFunctions,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.avatar_url = this.imagesHelpersFunctions.showImage(this.currentUser.avatar_link, 1,1, 500, 'avatar');
    // if (this.currentUser.avatar_link != null) {
    //   const tokens = this.currentUser.avatar_link.split('/');
    //   console.log(tokens);
    //   if (tokens[2] === 'res.cloudinary.com' && tokens[3] === 'photop') {
    //     const params = 'w_500,ar_1:1,c_fill,g_auto,e_art:hokusai';
    //     tokens.splice(-2, 0, params);
    //     this.avatar_url = tokens.join('/');
    //   } else {
    //     this.avatar_url = this.currentUser.avatar_link;
    //   }
    // } else {
    //   this.avatar_url = 'https://res.cloudinary.com/photop/image/upload/v1558442824/SPOTCREW/avatar.png';
    // }

  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe();
  }

  goToEditUserDataPage() {
    this.router.navigate(['/edit-user-data']);
  }

  goToChangePasswordPage() {
    this.router.navigate(['/change-password']);
  }

  showDialogToDeleteAccount() {
    this.display = true;
  }

  deleteAccount() {
    this.userService.delete(this.currentUser.id)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Delete account successful', true);
        },
        error => {
          this.alertService.error(error);
        });
    this.display = false;
    this.logout();
  }

  logout() {
    this.authenticationService.logout();
  }
}
