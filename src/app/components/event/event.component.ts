import { ImagesHelpersFunctions } from './../../_helpers';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Venue, Image, Event, Sport, User } from '../../_models';
import { VenuesService, AuthenticationService, AlertService, EventService, SportService, UserService } from '../../_services';

@Component({ templateUrl: 'event.component.html' })
export class EventComponent implements OnInit {

  venue: Venue = null;
  event: Event = null;
  host: User = null;
  loading = true;
  currentUser: User;
  currentUserSubscription: Subscription;
  loginUserData = false;
  loadingJoin = false;

  userIsJoin = false;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private venuesService: VenuesService,
    private alertService: AlertService,
    private eventService: EventService,
    private sportService: SportService,
    private userService: UserService,
    private imagesHelpersFunctions: ImagesHelpersFunctions,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.eventService.getById(id)
      .subscribe(data => {
        console.log(data);
        this.event = data;
        const url = this.sportService.returnSportImage(this.event.sport_id);
        this.event.sport_image = this.imagesHelpersFunctions.showImage(url, 2, 1, 500, 'sport');

        if (this.currentUser.id === this.event.host_id) {
          this.loginUserData = true;
        }

        this.event.users.forEach(user => {
          if (user.username === this.currentUser.username) {
            this.userIsJoin = true;
          }
          user.avatar_link = this.imagesHelpersFunctions.showImage(user.avatar_link, 1, 1, 200, 'avatar');
        });

        this.venuesService.getById(this.event.venue_id.toString())
          .subscribe( data => {
            console.log(data);
            this.venue = data;
            this.venue.images = this.imagesHelpersFunctions.showImageForImagesArray(this.venue.images, 2, 1, 500, 'venue');

            this.userService.getById(this.event.host_id)
              .subscribe( data => {
                this.host = data;
                this.host.avatar_link = this.imagesHelpersFunctions.showImage(this.host.avatar_link, 1, 1, 400, 'avatar');

                this.sportService.getById(this.event.sport_id.toString())
                  .subscribe( data => {
                    this.event.sport_name = data.name;
                    this.loading = false;
                  });
              });
          });
      });
  }

  createEvent() {
    this.router.navigate(['/create-event'], { queryParams: { venue_id: this.venue.venue_id } });
  }

  showVenue() {
    this.router.navigate(['/venue', this.event.venue_id]);
  }

  joinToEvent() {
    this.loadingJoin = true;
    this.eventService.attendanceCreateToEvent(this.event.event_id, this.currentUser.id)
      .subscribe(
        data => {
          console.log(data);
          this.currentUser.avatar_link = this.imagesHelpersFunctions.showImage(this.currentUser.avatar_link, 1, 1, 200, 'avatar');
          this.event.users.push(this.currentUser);
          this.userIsJoin = true;
          this.loadingJoin = false;
        },
        error => {
          this.alertService.error(error);
          this.loadingJoin = false;
        });
  }

  leaveToEvent() {
    this.loadingJoin = true;
    this.eventService.attendanceDeleteToEvent(this.event.event_id)
      .subscribe(
        data => {
          const newUser: User[] = [];
          this.event.users.forEach((user, index) => {
            if (user.username !== this.currentUser.username) {
              newUser.push(user);
            }
          });
          this.event.users = newUser;
          this.userIsJoin = false;
          this.loadingJoin = false;

        },
        error => {
          this.alertService.error(error);
          this.loadingJoin = false;
        });
  }

}
