import { ImagesHelpersFunctions } from './../../_helpers';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { OverlayPanel } from "primeng/primeng";

import { map } from 'rxjs/operators';
import { VenueModel, Venue, VenueListRespons, Image, Event, Sport } from '../../_models';
import { VenuesService, AuthenticationService, AlertService, HttpService, MapService, EventService, SportService } from '../../_services';

@Component({ templateUrl: 'events.component.html' })
export class EventsComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private venuesService: VenuesService,
    private eventService: EventService,
    private alertService: AlertService,
    private sportService: SportService,
    private http: HttpService,
    private map: MapService,
    private imagesHelpersFunctions: ImagesHelpersFunctions,
  ) {
  }

  loading = true;

  events: Event[];
  events_count: number;
  query: string;
  query_name: string;
  sports: Sport[];
  locationResults: Array<any>;

  ngOnInit() {
    this.map.findMe();

    this.eventService.getList()
      .subscribe(data => {
        this.events = data;
        this.events_count = this.events.length;

        this.events.forEach(event => {
          const url = this.sportService.returnSportImage(event.sport_id);
          event.sport_image = this.imagesHelpersFunctions.showImage(url, 2, 1, 500, 'sport', 3);
        });
        this.loading = false;
      });

    this.sportService.getList()
      .subscribe(
        data => {
          this.sports = data;
          console.log(this.sports);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  locationSearch(event) {
    return this.http.getLocalizationResults(event.query, this.map.getCurrentPosition())
      .subscribe((res: any) => {
        console.log(res);
        this.locationResults = res.results;
      });
  }

  searchEventInLocation(event) {
    console.log(event.position);
  }

  searchVenueForLocation() {
    // this.http.getLocalizationResults(this.query, this.map.getCurrentPosition())
    // .subscribe((res: any) => {
    //   console.log(this.query);
    //   console.log(res);
    //   console.log(res.results[0].position);
    //   let location: number[];
    //   if (typeof this.query !== 'string'){
    //     location = this.locationResults[0].position;
    //   } else {
    //     location = res.results[0].position;
    //   }
    //   console.log(location);
    //   this.venuesService.getListForLocation(10, 0, location, 30)
    //   .subscribe(data => {
    //     console.log(data);
    //     this.venues = data.results;
    //     this.venues_count = data.count;
    //     this.venues.forEach(venue => {
    //       console.log(venue);
    //       if (venue.images.length > 0) {
    //         const tokens = venue.images[0].link.split('/');
    //         console.log(tokens);
    //         if (tokens[2] === 'res.cloudinary.com' && tokens[3] === 'photop') {
    //           const params = 'w_500,ar_2:1,c_fill,g_auto';
    //           tokens.splice(-2, 0, params);
    //           venue.images[0].link = tokens.join('/');
    //         }
    //       } else {
    //         let image = new Image();
    //         image.link = 'https://res.cloudinary.com/photop/image/upload/w_500,ar_2:1,c_fill,g_auto/v1558442824/SPOTCREW/venue2.jpg';
    //         venue.images.push(image);
    //       }
    //     });
    //     this.loading = false;
    //   });
    // });
  }

  clearQuery() {
    this.query = '';
  }

  addVenue() {
    this.router.navigate(['/add-venue']);
  }

  showDetails(id: number) {
    this.router.navigate(['/event', id]);
  }

  onChange(selectSport) {
    console.log(selectSport);
}

  searchEventForName() {
    // this.venues = [];
    // this.loading = true;
    // this.venuesService.getListForName(10, 0, this.query_name)
    //   .subscribe(data => {
    //     console.log(data);
    //     this.venues = data.results;
    //     this.venues_count = data.count;
    //     this.venues.forEach(venue => {
    //       console.log(venue);
    //       if (venue.images.length > 0) {
    //         const tokens = venue.images[0].link.split('/');
    //         console.log(tokens);
    //         if (tokens[2] === 'res.cloudinary.com' && tokens[3] === 'photop') {
    //           const params = 'w_500,ar_2:1,c_fill,g_auto';
    //           tokens.splice(-2, 0, params);
    //           venue.images[0].link = tokens.join('/');
    //         }
    //       } else {
    //         let image = new Image();
    //         image.link = 'https://res.cloudinary.com/photop/image/upload/w_500,ar_2:1,c_fill,g_auto/v1558442824/SPOTCREW/venue2.jpg';
    //         venue.images.push(image);
    //       }
    //     });
    //     this.loading = false;
    //   });
  }

}
