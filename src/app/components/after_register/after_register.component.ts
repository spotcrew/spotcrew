import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({ templateUrl: 'after_register.component.html' })
export class AfterRegisterComponent implements OnInit {

  email = '[adress email]';

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.email = this.route.snapshot.paramMap.get('email');
  }

}
