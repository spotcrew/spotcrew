import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, HttpService } from '../../_services';

@Component({
  templateUrl: 'resetting_password.component.html'
})
export class ResettingPasswordComponent implements OnInit {
  resettingPasswordForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.resettingPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.resettingPasswordForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.resettingPasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.httpService.createResettingPassword(this.f.email.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success(`An email has been sent to ${ this.f.email.value }. Please check your email and use the enclosed link to finish resetting password.`, true);
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
